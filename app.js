const express = require('express')
const app = express()
const port = 3000;

const { Book } = require('./models')

app.use(express.json())

app.get('/books', (req, res) => {
    Book.findAll().then(books => {
      res.status(200).json({
        status: true,
        message: 'Books retrieved!',
        data: { books }
      })
    })
  })

  app.get('/books/:id', (req, res) => {    
    Book.findByPk(req.params.id).then(book => {    
      res.status(200).json({    
        status: true,
        message: `Books with ID ${req.params.id} retrieved!`,
        data: book
      })
    })
  })

app.post('/books', (req, res) => {
    Book.create({
      title: req.body.title,
      author: req.body.author
    }).then(book => {
      res.status(201).json({
        status: true,
        message: 'Books created!',
        data: book 
      })
    })
  })

  app.put('/books/:id', (req, res) => {
    Book.findByPk(req.params.id).then(book => {
      book.update({
        title: req.body.title,
        author: req.body.author
      }, {
        where: {
          id: req.params.id
        }
      }).then(() => {
        res.status(200).json({
          status: true,
          message: `Books with ID ${req.params.id} updated!`,
          data: book 
        })
      })
    })
  })
  
  app.delete('/books/:id', (req, res) => {
    Book.destroy({
      where: {
        id: req.params.id
      }
    }).then(() => {
      res.status(204).end()
    })
  })
  

app.listen(port, () => console.log(`Listening on port ${port}`))