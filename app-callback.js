const express = require('express');
const { Todo, User } = require('./models')
const app = express()
const port = 3000;

app.use(express.json())

class TodoController {
  static getAllTodo(callback) {
      Todo.findAll().then(todo => {
          return callback(null, todo)
      }).catch((e) => {
          return callback(e, null)
      })
  }

  static getTodoById(id, callback) {
      Todo.findByPk(id).then((todo) => {
          return callback(null, todo)
      }).catch((e) => {
          return callback(e, null)
      })
  }

  static postTodo(todo, callback) {
      Todo.create(todo).then((todo) => {
          return callback(null, todo)
      }).catch((e) => {
          return callback(e,null)
      })
  }

  static updateTodo(id, todo, callback) {
      Todo.update(todo, { where: { id } })
        .then((updatedTodo) => {
          return callback(null, updatedTodo);
        })
        .catch((error) => {
          return callback(error, null);
        });
    }

  static deleteTodo(id, callback) {
      Todo.destroy({ where: { id } }).then((todo) => {
          return callback(null, todo)
      }).catch((e) => {
          return callback(e, null)
      })
  }
}

app.get('/todos', (req, res) => {
    TodoController.getAllTodo((err,data) => {
        if(err) {
            res.status(400).json({
                message: err
            })
        } 
            res.status(200).json({
            message: 'Book retrieve',
            data: data
            })
    })
})

app.get('/todos/:id', (req, res) => {    
    const _id = req.params.id
    TodoController.getTodoById(_id, (err, data) => {
        if(err) {
          res.json(err)
        } else {
          res.status(200).json({
            status: true,
            message: `Activity with id ${_id} retrieved`,
            data: data,
          });
        }
    })
})  
  
app.post('/todos', (req, res) => {
    const todo = {
      name: req.body.name,
      description: req.body.description,
      due_at: req.body.due_at,
      user_id: req.body.user_id
    }
    TodoController.postTodo(todo, (err,data) => {
      if(err) {
        res.json(err)
      } else {
        if(!todo.user_id ) {
          res.status(400).json({
            message: 'You should insert user_id'
          })
        } else {
            res.status(201).json({
            status: true,
            message: 'Activities created!',
            data: todo
          })
        }
      }
    })
})

app.put('/todos/:id', (req, res) => {
  const _id = req.params.is
  TodoController.getTodoById(_id, (err, data) => {
    if(err) {
      res.json(err)
    } else {
      const todoUpdated = {
        name: data.name,
        description: data.description,
        due_at: data.due_at,
        user_id: data.user_id
      }
      TodoController.updateTodo(_id, todoUpdated, (err, data) => {
        if(err) {
          res.json(err) 
        } else {
          res.status(200).json({
            status: true,
            message: 'Todo updated',
            data: todoUpdated
          })
        }
      })
    }
  })
})

app.delete("/todos/:id", (req, res) => {
  const { id } = req.params;
  BookController.deleteBook(id, (error, book) => {
    if (error) {
      res.json(error);
    }

    res.status(201).json({
      status: true,
      message: `Todos with id ${id} deleted!`,
    });
  });
});

class UserController {
  static getAllUser(callback) {
    User.findAll({
      attributes: {
        exclude: ["password"],
      },
    })
      .then((users) => {
        return callback(null, users);
      })
      .catch((error) => {
        return callback(error, null);
      });
  }

  static getUser(id, callback) {
    User.findByPk(id)
      .then((user) => {
        return callback(null, user);
      })
      .catch((error) => {
        return callback(error, null);
      });
  }

  static createUser(user, callback) {
    User.create(user)
      .then((user) => {
        return callback(null, user);
      })
      .catch((error) => {
        return callback(error, null);
      });
  }

  static updateUser(id, user, callback) {
    User.update(user, { where: { id } })
      .then((updatedUser) => {
        return callback(null, updatedUser);
      })
      .catch((error) => {
        return callback(error, null);
      });
  }

  static deleteUser(id, callback) {
    User.destroy({
      where: {
        id,
      },
    })
      .then((user) => {
        return callback(null, user);
      })
      .catch((error) => {
        return callback(error, null);
      });
  }
}

app.get("/users", (req, res) => {
  UserController.getAllUser((error, users) => {
    if (error) {
      res.json(error);
    }

    if (users.length !== 0) {
      res.status(230).json({
        status: true,
        message: "All Users retrieved",
        data: users,
      });
    } else {
      res.status(244).json({
        status: false,
        message: `Users is empty`,
      });
    }
  });
});

app.get("/users/:id", (req, res) => {
  const { id } = req.params;
  UserController.getUser(id, (error, user) => {
    if (error) {
      res.json(error);
    }

    if (user && user.id) {
      res.status(230).json({
        status: true,
        message: `User with id ${id} retrieved`,
        data: user,
      });
    } else {
      res.status(244).json({
        status: false,
        message: `User with id ${id} not found`,
      });
    }
  });
});

app.post('/users', (req, res) => {
  const user = {
    name: req.body.name,
    email: req.body.email,
    password: req.body.password
  }

  UserController.createUser(user, (err, data) => {
    if(err) {
      res.json(err)
    } else {
      res.status(200).json ({
        status: true,
        message: 'User created',
        data: user
      })
    }
  })
})

app.put('/users/:id', (req, res) => {
  const _id = req.params.id
  UserController.getUser(_id, (err, data) => {
    if(err) {
      res.json(err)
    } else {
      const userUpdated = {
        name: data.name,
        email: data.email,
        password: data.password
      }
      UserController.updateUser(_id, userUpdated, (err,data) => {
        if(err) {
          res.json(err)
        } else {
          res.status(200).json({
            status:true,
            message: 'Users updated',
            data: userUpdated
          })
        }
      })
    }
  })
})

app.delete('/Users/:id', (req, res) => {
  const _id = req.params.id
  UserController.deleteUser(_id, (err,data) => {
    if(err) {
      res.json(err)
    } else {
      res.status(201).json({
        status: true,
        message: `User with ${_id} is deleted`,
        data: data
      })
    }
  })
})
  
app.listen(port, () => console.log(`Listening on port ${port}`))
