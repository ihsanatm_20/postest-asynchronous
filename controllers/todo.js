const { Todo } = require('./models')

class TodoController {
    static getAllTodo(callback) {
        Todo.findAll().then(todo => {
            return callback(null, todo)
        }).catch((e) => {
            return callback(e, null)
        })
    }

    static getTodoById(id, callback) {
        Todo.findByPk(id).then((todo) => {
            return callback(null, todo)
        }).catch((e) => {
            return callback(e, null)
        })
    }

    static postTodo(todo, callback) {
        Todo.create(todo).then((todo) => {
            return callback(null, todo)
        }).catch((e) => {
            return callback(e,null)
        })
    }

    static updateTodo(id, todo, callback) {
        Todo.update(todo, { where: { id } })
          .then((updatedTodo) => {
            return callback(null, updatedTodo);
          })
          .catch((error) => {
            return callback(error, null);
          });
      }

    static deleteTodo(id, callback) {
        Todo.destroy({ where: { id } }).then((todo) => {
            return callback(null, todo)
        }).catch((e) => {
            return callback(e, null)
        })
    }
}

module.exports = TodoController

