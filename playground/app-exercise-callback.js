const express = require('express');

const app = express();
const port = process.env.PORT || 8001;
app.use(express.json())
const { Op } = require("sequelize");

const { Todo } = require('./models');
const { User } = require('./models');


  
// fungsi todos ===============================================

    app.get('/todos',  (req, res) => {
        const getTodos = function (callback) {
            Todo.findAll({order: [['id', 'ASC']]}).then(data => {
                okresult = { status: true,
                    message: `todos retrieved!`,
                    data};
                return callback(null, okresult);
            })
            .catch((err) => {
                errresult = {
                    status: false,
                    message: err.message
                    };
                return callback(errresult);
            });
        }
        
        getTodos(function(err, dataresult) {
            if (err) {
                res.status(422).json(err)
            } 
            else {
                res.status(200).json(dataresult)
            }
         })
    })


    app.get('/todos/:id', (req, res) => {
        const getTodoByID = function (callback) {
            Todo.findByPk(req.params.id).then(data => {
                if (data === null) {
                    okresult = {
                        status: false,
                        message: `todos with ID ${req.params.id} not Found!`,
                        data
                    }
                    return callback(null, okresult);
                } 
                else {
                    okresult = {
                        status: true,
                        message: `todos with ID ${req.params.id} retrieved!`,
                        data
                    }
                    return callback(null, okresult);
                }
            })
            .catch((err) => {
                errresult = {
                    status: false,
                    message: err.message
                }
                return callback(errresult);
            }) 
        }

        getTodoByID(function(err, dataresult) {
            if (err) {
                res.status(422).json(err)
            } 
            else {
                res.status(200).json(dataresult)
            }
         })
    })
  
        
    app.post('/todos', (req, res) => {
        const insertTodo = function (callback) {
            Todo.findOne({ where: { id: req.body.user_id } })
            .then(findIdUser => {
                if (findIdUser === null) {
                    okresult = {
                        status: false,
                        message: 'Can not insert todos because user id not found!',
                    }
                    return callback(null, okresult);
                } 
                else {
                    Todo.create({
                        name: req.body.name,
                        description: req.body.description,
                        due_at :req.body.due_at,
                        user_id: req.body.user_id
                    })
                    .then(data => {
                        okresult = {
                            status: true,
                            message: 'todos created!',
                            data 
                        }
                        return callback(null, okresult);
                    })
                    .catch((err) => {
                        errresult = {
                        status: false,
                        message: err.message
                        }
                        return callback(errresult)
                    })    
                }
            })
            .catch((err) => {
                errresult = {
                status: false,
                message: err.message
                }
                return callback(errresult)
            })
        }

        insertTodo(function(err, dataresult) {
            if (err) {
                res.status(422).json(err)
            } 
            else {
                res.status(200).json(dataresult)
            }
         })
    })     


    app.delete('/todos/:id', (req, res) => {
        const deleteTodo = function (callback) {
            Todo.destroy({where: {id: req.params.id}})
            .then(n => {
                if (n == 0) {
                    okresult = {
                        status: false,
                        message: `todos with id ${req.params.id} not found!`
                    }
                    return callback(null, okresult);
                }
                else {
                    okresult = {
                        status: true,
                        message: `${n} rows executed!! todos with id ${req.params.id} deleted!`
                    }
                    return callback(null, okresult);
                }
            })
            .catch((err) => {
                errresult = {
                    status: false,
                    message: err.message
                }
                return callback(errresult)
            })
        }

        deleteTodo(function(err, dataresult) {
            if (err) {
                res.status(422).json(err)
            } 
            else {
                res.status(200).json(dataresult)
            }
         })
    })



    app.put('/todos/:id', (req, res) => {
        const updateTodo = function (callback) {  
            Todo.findByPk(req.params.id)
            .then(data => {
                if (data === null) {
                    okresult = {
                        status: false,
                        message: `todos with ID ${req.params.id} not Found!`,
                        data
                    }
                    return callback(null, okresult);
                } 
                else {
                    Todo.findOne({ where: { id: req.body.user_id } })
                    .then(findIdUser => {
                        if (findIdUser === null) {
                            okresult = {
                                status: false,
                                message: 'Can not update todos because user id not found!',
                            }
                            return callback(null, okresult);
                        } 
                        else { 
                            Todo.update({
                                name: (req.body.name == null || req.body.name == "") ? data.name : req.body.name,
                                description: (req.body.description == null || req.body.description == "") ? data.description : req.body.description,
                                due_at: (req.body.password == null || req.body.password == "") ? data.due_at : req.body.due_at,
                                user_id:(req.body.user_id == null || req.body.user_id == "") ? data.user_id : req.body.user_id
                            }, {where: {id: req.params.id}}
                            )
                            .then(dataupdate => { 
                                Todo.findByPk(req.params.id)
                                .then(datanew => {
                                    if (dataupdate == 1 && datanew ) {
                                        okresult = {
                                            status: true,
                                            message: `${dataupdate} rows executed!! todos with ID ${req.params.id} updated!`,
                                            data,
                                            datanew
                                        }
                                        return callback(null, okresult);
                                    }
                                })
                            })
                            .catch((err) => {
                                errresult = {
                                    status: false,
                                    message: err.message
                                }
                                return callback(errresult)
                            }) 
                        }
                    })
                    .catch((err) => {
                        errresult = {
                            status: false,
                            message: err.message
                        }
                        return callback(errresult)
                    })
                }
            })    
            .catch((err) => {
                errresult = {
                    status: false,
                    message: err.message
                }
                return callback(errresult)
            })
        }

        updateTodo(function(err, dataresult) {
            if (err) {
                res.status(422).json(err)
            } 
            else {
                res.status(200).json(dataresult)
            }
         })

    })    

// fungsi users ===============================================
  
    app.get('/users', (req, res) => {
        const getUsers = function (cb) {
            User.findAll({order: [['id', 'ASC']]})
            .then(data => {
                okresult = {
                    status: true,
                    message: `users retrieved!`,
                    data
                }
                return cb(null, okresult);
            })
            .catch((err) => {
                errresult = {
                    status: false,
                    message: err.message
                }
                return cb(errresult)
            })
        }

        getUsers(function(err, dataresult) {
            if (err) {
                res.status(422).json(err)
            } 
            else {
                res.status(200).json(dataresult)
            }
         })
    })


    app.get('/users/:id',  (req, res) => {
        const getUserByID = function (cb) {
            User.findByPk(req.params.id)
            .then(data => {
                if (data === null) {
                    okresult = {
                        status: false,
                        message: `users with ID ${req.params.id} not Found!`,
                        data
                    }
                    return cb(null, okresult);
                } 
                else {
                    okresult = {
                        status: true,
                        message: `users with ID ${req.params.id} retrieved!`,
                        data
                    }
                    return cb(null, okresult);
                } 
            })
            .catch((err) => {
                errresult = {
                    status: false,
                    message: err.message
                }
                return cb(errresult)
            })
        }

        getUserByID(function(err, dataresult) {
            if (err) {
                res.status(422).json(err)
            } 
            else {
                res.status(200).json(dataresult)
            }
         })
    })


    app.post('/users',  (req, res) => {
        const insertUser = function (cb) {
            User.findOrCreate({
                where: {email: req.body.email}, 
                defaults: {
                    name: req.body.name,
                    email: req.body.email,
                    password: (req.body.password == null || req.body.password == "") ?'tsel1234' : req.body.password                    
                }
            })
            .then((data) => {
                if (data[1]) {
                    okresult = {
                        status: true,
                        message: 'user created!',
                        data : data[0],
                    
                    }
                    return cb(null, okresult);
                }
                else {
                    okresult = {
                        status: false,
                        message: 'email already exist!',  
                    }
                    return cb(null, okresult);
                }
            })
            .catch((err) => {
                errresult = {
                    status: false,
                    message: err.message
                }
                return cb(errresult)
            })
        }

        insertUser(function(err, dataresult) {
            if (err) {
                res.status(422).json(err)
            } 
            else {
                res.status(200).json(dataresult)
            }
         })
    })   


    app.delete('/users/:id',  (req, res) => {
        const deleteUser = function (cb) {
            User.destroy({where: {id: req.params.id}})
            .then(n => {
                if (n == 0) {
                    okresult = {
                        status: false,
                        message: `user with id ${req.params.id} not found!`
                    }
                    return cb(null, okresult);
                }
                else {
                    okresult = {
                        status: true,
                        message: `${n} rows executed!! user with id ${req.params.id} deleted!`
                    }
                    return cb(null, okresult);
                }
            })
            .catch((err) => {
                errresult = {
                    status: false,
                    message: err.message
                }
                return cb(errresult)
            })
        }

        deleteUser(function(err, dataresult) {
            if (err) {
                res.status(422).json(err)
            } 
            else {
                res.status(200).json(dataresult)
            }
         })
    })
  

    app.put('/users/:id', (req, res) => {
        const updateUser = function (cb) {
            User.findByPk(req.params.id)
            .then(data => {
                if (data === null) {
                    okresult = {
                        status: false,
                        message: `users with ID ${req.params.id} not Found!`,
                        data
                    }
                    return cb(null, okresult);
                } 
                else {
                    User.findOne({ 
                        where: { 
                        email: req.body.email, 
                        id: {
                            [Op.ne]: req.params.id
                        }
                        }
                    })
                    .then(findEmailUser => {
                        if (findEmailUser != null) {
                            okresult = {
                                status: false,
                                message: 'Can not update users because email already exist in other user!',
                            }
                            return cb(null, okresult);
                        } 
                        else {    
                            User.update({
                                name: (req.body.name == null || req.body.name == "") ? data.name : req.body.name,
                                email: (req.body.email == null || req.body.email == "") ? data.email : req.body.email,
                                password: (req.body.password == null || req.body.password == "") ? data.password : req.body.password
                            }, {where: {id: req.params.id}}
                            )
                            .then(dataupdate => {
                                User.findByPk(req.params.id)
                                .then(datanew => {
                                    if (dataupdate == 1 && datanew ) {
                                        okresult = {
                                            status: true,
                                            message: `${dataupdate} rows executed!!  users with ID ${req.params.id} updated!`,
                                            data,
                                            datanew
                                        }
                                        return cb(null, okresult);
                                    }
                                })
                                .catch((err) => {
                                    errresult = {
                                        status: false,
                                        message: err.message
                                    }
                                    return cb(errresult)
                                })
                            })
                            .catch((err) => {
                                errresult = {
                                    status: false,
                                    message: err.message
                                }
                                return cb(errresult)
                            }) 
                        }
                    })
                    .catch((err) => {
                        errresult = {
                            status: false,
                            message: err.message
                        }
                        return cb(errresult)
                    })      
                }
            })
            .catch((err) => {
                errresult = {
                    status: false,
                    message: err.message
                }
                return cb(errresult)
            })
        }

        updateUser(function(err, dataresult) {
            if (err) {
                res.status(422).json(err)
            } 
            else {
                res.status(200).json(dataresult)
            }
         })
    })


app.listen(port, () => { console.log(` app listening at http://localhost:${port}`)});