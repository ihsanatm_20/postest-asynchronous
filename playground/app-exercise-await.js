const express = require('express');

const app = express();
const port = process.env.PORT || 8001;
app.use(express.json())

const { Todo } = require('./models');
const { User } = require('./models');

// fungsi todos ===============================================
 
  app.get('/todos', async (req, res) => {
  try {
  const data = await Todo.findAll({
    order: [
      ['id', 'ASC']
  ]
  });
      res.status(200).json({
      status: true,
      message: `todos retrieved!`,
      data
    })
  }
  catch(err) {
      res.status(422).json({
      status: false,
      message: err.message
    })
  }
  })


  app.get('/todos/:id', async (req, res) => {
    try {
    const data = await Todo.findByPk(req.params.id);
     if (data === null) {
      res.status(202).json({
        status: false,
        message: `todos with ID ${req.params.id} not Found!`,
        data
      })
     } 
     else {
      res.status(200).json({
        status: true,
        message: `todos with ID ${req.params.id} retrieved!`,
        data
      })
     } 
    }
    catch(err) {
      res.status(422).json({
        status: false,
        message: err.message
      })
    }
    })
  

  app.post('/todos', async (req, res) => {
  try {
  const findIdUser = await Todo.findOne({ where: { id: req.body.user_id } });
  if (findIdUser === null) {
    res.status(201).json({
      status: false,
      message: 'Can not insert todos because user id not found!'
    })
  } else {
    Todo.create({
      name: req.body.name,
      description: req.body.description,
      due_at :req.body.due_at,
      user_id: req.body.user_id
    }).then(todo => {
      res.status(201).json({
        status: true,
        message: 'todos created!',
        data: todo 
      })  
    })
    }   
  }
  catch(err) {
    res.status(422).json({
      status: false,
      message: err.message
    })
  }
  })


  app.delete('/todos/:id', async (req, res) => {
    try {
    let n = await Todo.destroy({
      where: {
        id: req.params.id
      }
    });
    if (n == 0) {
      res.status(202).json({
        status: false,
        message: `todos with id ${req.params.id} not found!`
      })
    }
    else {
      res.status(200).json({
        status: true,
        message: `${n} rows executed!! todos with id ${req.params.id} deleted!`
      })
    }
   }
   catch(err) {
    res.status(422).json({
      status: false,
      message: err.message
    })
  }
  })



  app.put('/todos/:id', async (req, res) => {
    try { 
     const data = await Todo.findByPk(req.params.id);
     if (data === null) {
      res.status(202).json({
        status: false,
        message: `todos with ID ${req.params.id} not Found!`,
        data
      })
     } 
     else {
      const findIdUser = await user.findOne({ where: { id: req.body.user_id } });
      if (findIdUser === null) {
        res.status(201).json({
        status: false,
        message: 'Can not update todos because user id not found!',
        })
      } else { 
      let dataupdate = await Todo.update({
        name: (req.body.name == null || req.body.name == "") ? data.name : req.body.name,
        description: (req.body.description == null || req.body.description == "") ? data.description : req.body.description,
        due_at: (req.body.password == null || req.body.password == "") ? data.due_at : req.body.due_at,
        user_id:(req.body.user_id == null || req.body.user_id == "") ? data.user_id : req.body.user_id
      }, {
        where: {
          id: req.params.id
        }
      })
      const datanew = await Todo.findByPk(req.params.id);
      if (dataupdate == 1 && datanew ) {
        res.status(200).json({
        status: true,
        message: `${dataupdate} rows executed!! todos with ID ${req.params.id} updated!`,
        data,
        datanew
      })
      }
      }
     } 
    }
    catch(err) {
      res.status(422).json({
        status: false,
        message: err.message
      })
    }
    })

// fungsi users ===============================================
  
 app.get('/users', async (req, res) => {
  try {
  const data = await User.findAll({
    order: [
      ['id', 'ASC']
  ]
  });
    res.status(200).json({
      status: true,
      message: `users retrieved!`,
      data
    })
  }
  catch(err) {
    res.status(422).json({
      status: false,
      message: err.message
    })
  }
  })


  app.get('/users/:id', async (req, res) => {
  try {
   const data = await User.findByPk(req.params.id);
   if (data === null) {
    res.status(202).json({
      status: false,
      message: `users with ID ${req.params.id} not Found!`,
      data
    })
   } 
   else {
    res.status(200).json({
      status: true,
      message: `users with ID ${req.params.id} retrieved!`,
      data
    })
   } 
  }
  catch(err) {
    res.status(422).json({
      status: false,
      message: err.message
    })
  }
  })


  app.post('/users', async (req, res) => {
    try {
     const [data, created] = await User.findOrCreate({
          where: {email: req.body.email}, 
          defaults: {
                name: req.body.name,
                email: req.body.email,
                password: (req.body.password == null || req.body.password == "") ?'tsel1234' : req.body.password                    
                    }
        });
        if (created) {
          res.status(201).json({
            status: true,
            message: 'user created!',
            data
          })
        }
        else {
          res.status(202).json({
            status: false,
            message: 'email already exist!'
          })
        }
      }
    catch(err) {
        res.status(422).json({
          status: false,
          message: err.message
        })
      }
      })   


  app.delete('/users/:id', async (req, res) => {
  try { 
    let n = await User.destroy({
      where: {
        id: req.params.id
      }
    });
    if (n == 0) {
      res.status(202).json({
        status: false,
        message: `user with id ${req.params.id} not found!`
      })
    }
    else {
      res.status(200).json({
        status: true,
        message: `${n} rows executed!! user with id ${req.params.id} deleted!`
      })
    }
  }
  catch(err) {
    res.status(422).json({
      status: false,
      message: err.message
    })
  }
  })
  

  app.put('/users/:id', async (req, res) => {
    try {
     const data = await User.findByPk(req.params.id);
     if (data === null) {
      res.status(202).json({
        status: false,
        message: `users with ID ${req.params.id} not Found!`,
        data
      })
     } 
     else {
      let dataupdate = await User.update({
        name: (req.body.name == null || req.body.name == "") ? data.name : req.body.name,
        email: (req.body.email == null || req.body.email == "") ? data.email : req.body.email,
        password: (req.body.password == null || req.body.password == "") ? data.password : req.body.password
      }, {
        where: {
          id: req.params.id
        }
      })
      const datanew = await User.findByPk(req.params.id);
      if (dataupdate == 1 && datanew ) {
      res.status(200).json({
        status: true,
        message: `${dataupdate} rows executed!!  users with ID ${req.params.id} updated!`,
        data,
        datanew
      })
      }
     } 
    }
    catch(err) {
      res.status(422).json({
        status: false,
        message: err.message
      })
    }
    })



app.listen(port, () => { console.log(` app listening at http://localhost:${port}`)});